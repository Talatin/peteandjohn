using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float turnSpeed;
    private PlayerInputActions playerInputActions;
    private InputAction movement;
    private InputAction aim;
    private Vector2 mouseDir;
    private Rigidbody rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        playerInputActions = new PlayerInputActions();
    }

    private void OnEnable()
    {
        movement = playerInputActions.Player.Movement;
        movement.Enable();
        aim = playerInputActions.Player.Aim;
        aim.performed += GetMouseLookDirection;
        aim.Enable();
    }

    private void GetMouseLookDirection(InputAction.CallbackContext ctx)
    {
        mouseDir = ctx.ReadValue<Vector2>().normalized;
        Vector3 lookDir = new Vector3(mouseDir.x, 0, mouseDir.y);
       // transform.LookAt(transform.position + lookDir, Vector3.up);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler((transform.position + lookDir) - transform.position), turnSpeed * Time.deltaTime);
    }

    private void OnDisable()
    {
        movement.Disable();
        aim.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void Update()
    {
    }
    private void FixedUpdate()
    {
        Vector3 velocity = new Vector3(movement.ReadValue<Vector2>().x, rb.velocity.y, movement.ReadValue<Vector2>().y);
        rb.velocity = velocity * speed * Time.fixedDeltaTime;
    }
}
